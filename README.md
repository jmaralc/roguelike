# Roguelike

This is just a repo to develop a rogue-like game with Rust.

Why Rust? Because it has potential, and I want to start using it in my daily work. 

Why a game? Because it is better to start with something fun and I've never developed a game (but always want it secretly).

This is based on the work of **Herbert Wolverson** and his book [Hands on Rust](https://g.co/kgs/CQHGha)

The graphic resources are here thanks to:

- Dungeon floor, wall and adventurer -> [buch for free](https://opengameart.org/content/unfinished-dungeon-tileset)
- Potion and scroll -> [Melissa Kraytheim's fantasy Magic set](https://opengameart.org/content/fantasy-magic-set)
- Weaponry -> [Melle's Fantasy Sword Set](https://opengameart.org/content/fantasy-sword-set)
- Monsters -> Chris Hamons, [Dungeon Crawl Stone Soup](https://github.com/crawl/tiles)

Thanks all the people mentioned above for their contribution and for teaching me. 